#!/bin/bash
set -e

#Go to root directory
BASEDIR=$(dirname "$0")

#Account variables
AWS_REGION="ap-south-1"
REGISTRY=""

#App specific variables
ecr_repo_name="bytescope"


cd $BASEDIR/../
git_commit=$(git rev-parse --short HEAD)
git_branch=${BRANCH_NAME}
epoch=$(date +%s)

#For building docker images locally
local=0
if [ "$git_branch" = "" ];then
   echo "Branch name not found in env variables"
   git_branch="$(git rev-parse --abbrev-ref HEAD)"
   local=1    
fi

tag="${git_branch}-${git_commit}-${epoch}"
echo "${REGISTRY}/${ecr_repo_name}" > registry.txt
echo "$tag" > docker-tag.txt

$(aws ecr get-login --no-include-email --region ${AWS_REGION})

echo "*** Building Docker image: ${REGISTRY}/${ecr_repo_name}:${tag} ***"
docker build --build-arg git_commit_id=${git_commit} -t ${REGISTRY}/${ecr_repo_name}:${tag} .
echo "Image built successfully".

if [ $local = 0 ];then
  
   echo "Pushing image to Docker Registry."
   docker push ${REGISTRY}/${ecr_repo_name}:${tag}
   echo "Image pushed successfully."

   docker rmi -f ${REGISTRY}/${ecr_repo_name}:${tag}
   echo "Deleted image from build server."
fi

