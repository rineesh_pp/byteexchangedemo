const path = require("path");
module.exports = {
  publicPath: "/",
  assetsDir: "static",
  lintOnSave: process.env.NODE_ENV !== "production",
  devServer: {
    disableHostCheck: true,
    proxy: "http://localhost:8080"
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import '@/scss/common.scss';`
      }
    }
  },
  chainWebpack: config => {
    const inlineLimit = 10000;
    const assetsPath = "@/assets/fonts/";
    config.module
      .rule("fonts")
      .test(/\.(woff2?|eot|ttf|otf|woff)(\?.*)?$/i)
      .use("url-loader")
      .loader("url-loader")
      .options({
        limit: inlineLimit,
        name: path.join(assetsPath, "[name].[hash:8].[ext]")
      });
  }
};
