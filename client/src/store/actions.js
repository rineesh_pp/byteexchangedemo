const Actions = {
  setApiInfo({ commit }, val) {
    if (val) {
      commit("SET_API_INFO", { ...val });
      setTimeout(() => {
        commit("SET_API_INFO", null);
      }, 3000);
    } else {
      commit("SET_API_INFO", null);
    }
  }
};
export default { ...Actions };
