import mutations from "./mutations";
import actions from "./actions";

const state = {
  isLoading: false,
  reportsData: null
};
const getters = {
  gstReportsData: ({ reportsData }) => reportsData?.csv
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
