import apiService from "@/services/apiService";
import { apiURLConfig } from "@/constants";
import { headTypeItemObj } from "@/constants/financial";
import map from "lodash/map";
import moment from "moment";
const { finReconUrl, finHeadTypesUrl, finEntryUrl } = apiURLConfig;
const Actions = {
  getFinReconData: ({ state, commit, dispatch, rootGetters }) => {
    let { getFilterParamsWithValue } = rootGetters;
    let { fromDate, toDate } = getFilterParamsWithValue;
    let { sortItem: sort } = state;
    fromDate = fromDate ? moment(fromDate).format("YYYY-MM-DD") : fromDate;
    toDate = toDate ? moment(toDate).format("YYYY-MM-DD") : toDate;
    commit("SET_LOADER", true);
    apiService
      .get({
        url: finReconUrl,
        params: {
          ...getFilterParamsWithValue,
          fromDate,
          toDate,
          sort
        }
      })
      .then(res => {
        commit("SET_LOADER", false);
        if (res?.data?.data) {
          commit("SET_FINRECON_DATA", res?.data?.data);
        } else {
          dispatch(
            "setApiInfo",
            { status: "success", message: "No records found." },
            { root: true }
          );
        }
      })
      .catch(err => {
        commit("SET_LOADER", false);
        commit("SET_FINRECON_DATA", null);
        if (err?.message !== "CANCELLED") {
          const message = err?.meta?.message || "Something went wrong. :(";
          dispatch("setApiInfo", { status: "error", message }, { root: true });
        }
      });
  },
  getFinHeadTypes: ({ commit, dispatch }) => {
    commit("SET_LOADER", true);
    apiService
      .get({
        url: finHeadTypesUrl
      })
      .then(res => {
        commit("SET_LOADER", false);
        if (res?.data?.data) {
          let options = mapheadTypes(res.data.data);
          commit("SET_FIN_HEADTYPES", options);
        } else {
          dispatch(
            "setApiInfo",
            { status: "success", message: "No head types found." },
            { root: true }
          );
        }
      })
      .catch(err => {
        commit("SET_LOADER", false);
        if (err?.message !== "CANCELLED") {
          const message = err?.meta?.message || "Something went wrong. :(";
          dispatch("setApiInfo", { status: "error", message }, { root: true });
        }
      });
  },
  postEntryData: ({ state, rootState, dispatch, commit }) => {
    commit("SET_LOADER", true);
    const {
      userDetails: { userInfo }
    } = rootState;
    apiService
      .post({
        url: finEntryUrl,
        headers: {
          reconUserName: userInfo?.user_id,
          reconUserId: userInfo?.agent_id
        },
        data: { ...state.entryPayload }
      })
      .then(res => {
        if (res?.data?.data === "SUCCESS") {
          dispatch(
            "setApiInfo",
            {
              message: "Entry added successfuly",
              status: "success"
            },
            { root: true }
          );
          commit("RESET_ENTRY_PAYLOAD");
          dispatch("getFinReconData");
        } else {
          throw res;
        }
        commit("SET_LOADER", false);
      })
      .catch(err => {
        commit("SET_LOADER", false);
        if (err?.message !== "CANCELLED") {
          const message = err?.meta?.message || "Something went wrong. :(";
          dispatch("setApiInfo", { status: "error", message }, { root: true });
        }
      });
  }
};
const mapheadTypes = data => {
  let options = [];
  map(data, resItem => {
    let item = null;
    item = headTypeItemObj.hasOwnProperty(resItem)
      ? headTypeItemObj[resItem]
      : null;
    if (item) {
      options.push(item);
    }
  });
  return options;
};
export default { ...Actions };
