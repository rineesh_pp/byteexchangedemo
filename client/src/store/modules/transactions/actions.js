import moment from "moment";
import apiService from "@/services/apiService";
import { apiURLConfig } from "@/constants";

const { transactionsUrl } = apiURLConfig;
const Actions = {
  getTransactionsData: (
    { state, commit, dispatch, rootGetters, rootState },
    { searchKey } = {}
  ) => {
    let { getFilterParamsWithValue, getSearchParamsWithValue } = rootGetters;
    let { signal } = rootState;
    let { sortItem: sort } = state;
    let params = {
      sort,
      signal
    };
    if (searchKey) {
      params = {
        ...params,
        ...getSearchParamsWithValue
      };
    } else {
      let { fromDate, toDate } = getFilterParamsWithValue;
      fromDate = fromDate ? moment(fromDate).format("YYYY-MM-DD") : fromDate;
      toDate = toDate ? moment(toDate).format("YYYY-MM-DD") : toDate;
      params = {
        ...params,
        ...getFilterParamsWithValue,
        fromDate,
        toDate
      };
    }
    commit("SET_LOADER", true);
    apiService
      .get({
        url: transactionsUrl,
        params
      })
      .then(res => {
        commit("SET_LOADER", false);
        if (res?.data?.data) {
          commit("SET_TRANSACTIONS_DATA", res?.data?.data);
        } else {
          dispatch(
            "setApiInfo",
            { status: "success", message: "No records found." },
            { root: true }
          );
        }
      })
      .catch(err => {
        commit("SET_LOADER", false);
        commit("SET_TRANSACTIONS_DATA", null);
        if (err?.message !== "CANCELLED") {
          const message = err?.meta?.message || "Something went wrong :(";
          dispatch("setApiInfo", { status: "error", message }, { root: true });
        }
      });
  }
};

export default { ...Actions };
