import mutations from "./mutations";
import actions from "./actions";

const state = {
  isLoading: false,
  transactionsData: null,
  sortItem: null
};

const getters = {
  getPurchaseCardData: ({ transactionsData }) => {
    const {
      transactionCount,
      investmentAmount,
      contributionAmount,
      accountOpeningFees,
      paymentFees
    } = transactionsData?.statistics || {};
    return [
      {
        value: transactionCount || "0",
        label: "Transaction Count",
        isInr: false
      },
      {
        value: investmentAmount || "0.0",
        label: "Investment Amount",
        isInr: true
      },
      {
        value: contributionAmount || "0.0",
        label: "NPS Contribution",
        isInr: true
      },
      {
        value: accountOpeningFees || "0.0",
        label: "Account opening Fee",
        isInr: true
      },
      {
        value: paymentFees || "0.0",
        label: "Payment fee",
        isInr: true
      }
    ];
  },
  getRedemptionCardData: ({ transactionsData }) => {
    const { transactionCount, redemptionAmount } =
      transactionsData?.statistics || {};
    return [
      {
        value: transactionCount || "0",
        label: "Transaction Count",
        isInr: false
      },
      {
        value: redemptionAmount || "0.0",
        label: "Redemption Amount",
        isInr: true
      }
    ];
  },
  getRevenueCardData: ({ transactionsData }) => {
    const {
      transactionCount,
      numberOfInvestors,
      netRevenueAmount,
      refundedAmount
    } = transactionsData?.statistics || {};
    return [
      {
        value: transactionCount || "0",
        label: "Transaction Count",
        isInr: false
      },
      {
        value: numberOfInvestors || "0",
        label: "No. of Investors",
        isInr: false
      },
      {
        value: netRevenueAmount || "0.0",
        label: "Net Revenue Amount",
        isInr: true
      },
      {
        value: refundedAmount || "0.0",
        label: "Refunded Amount",
        isInr: true
      }
    ];
  },
  getTableData: ({ transactionsData }) => {
    return transactionsData?.tableData;
  },
  getTotalPages: ({ transactionsData }) => {
    return transactionsData?.totalPages;
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
