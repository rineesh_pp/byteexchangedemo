import mutations from "./mutations";
import actions from "./actions";

const state = {
  isLoading: false,
  trialBalanceData: null,
  sortItem: null
};
const getters = {
  getCardData: ({ trialBalanceData }) => {
    const {
      accuralAmount,
      payableAmount,
      amountToPFRDA,
      pgReceivable,
      collectionBankBalance,
      revenueBankBalance,
      bankBalanceTotal,
      registrationCharges,
      paymentCharges,
      revenuBalanceTotal,
      gstPayable
    } = trialBalanceData || {};
    return [
      {
        value: amountToPFRDA || "0.0",
        label: "Investment Amount payable To PFRDA",
        isInr: true,
        subItem: [
          { value: accuralAmount || "0.0", label: "Accural", isInr: true },
          { value: payableAmount || "0.0", label: "Payable", isInr: true }
        ]
      },
      { value: pgReceivable || "0.0", label: "PG Receivable", isInr: true },
      {
        value: bankBalanceTotal || "0.0",
        label: "Bank Balance",
        isInr: true,
        subItem: [
          {
            value: collectionBankBalance || "0.0",
            label: "Collection",
            isInr: true
          },
          { value: revenueBankBalance || "0.0", label: "Revenue", isInr: true }
        ]
      },
      {
        value: revenuBalanceTotal || "0.0",
        label: "Revenue Balance",
        isInr: true,
        subItem: [
          {
            value: registrationCharges || "0.0",
            label: "A/c Opening Fee",
            isInr: true
          },
          { value: paymentCharges || "0.0", label: "Payment Fee", isInr: true }
        ]
      },
      { value: gstPayable || "0.0", label: "GST Payable", isInr: true }
    ];
  },
  getTableData: ({ trialBalanceData }) => {
    return trialBalanceData?.tableData;
  },
  getTotalPages: ({ trialBalanceData }) => {
    return trialBalanceData?.totalPages;
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
