export const capitalize = value => {
  if (!value) {
    return "";
  }
  value = value.toString();
  return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
};
export const uppercase = value => {
  if (!value) {
    return "";
  }
  value = value.toString();
  return value.toUpperCase();
};
export const iconizeInr = val => {
  let value = val ? val.toString() : "";
  if (!value || (value && value.indexOf("INR") == -1)) {
    return value;
  } else {
    return value.replace("INR", "₹");
  }
};
export const localCurrency = val => {
  return val
    ? Number(val).toLocaleString("en-IN", {
        style: "currency",
        currency: "INR"
      })
    : 0;
};
export const getCookie = name => {
  const re = new RegExp(`${name}=([^;]+)`);
  const value = re.exec(document.cookie);
  return value != null ? unescape(value[1]) : null;
};
export const removeCookie = name => {
  document.cookie = `${name}=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/;`;
};
export const formatValue = ({ value, isINR = false }) => {
  let strVal = value ? value.toString() : "0";
  let newVal = strVal.split(".")[0];
  if (Number(newVal) === 0) return Number(newVal);
  if (newVal.length === 7) {
    newVal = newVal.substring(0, 4);
    newVal = parseInt(newVal) / 100;
    newVal = newVal % 10 === 0 ? Math.round(newVal) : newVal;
    newVal = `${newVal}L`;
  } else if (newVal.length > 7) {
    newVal = newVal.substring(0, newVal.length - 5);
    newVal = parseInt(newVal) / 100;
    newVal =
      newVal % 10 === 0
        ? Math.round(newVal).toLocaleString("en-IN")
        : newVal.toLocaleString("en-IN");
    newVal = `${newVal}Cr`;
  } else {
    newVal = parseInt(newVal)
      .toLocaleString("en-IN")
      .toString();
  }

  if (isINR) {
    newVal = `₹ ${newVal}`;
  }

  return newVal;
};
