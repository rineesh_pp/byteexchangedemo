export const mockTableData = {
  rowHeader: [
    {
      id: "test1",
      title: "First "
    },
    {
      id: "test2",
      title: "Second "
    },
    {
      id: "test3",
      title: "Third "
    },
    {
      id: "test4",
      title: "Fourth "
    },
    {
      id: "test5",
      title: "Fifth "
    }
  ],
  rowData: [
    {
      test1: "1",
      test2: "2",
      test3: "3",
      test4: "4",
      test5: "5"
    },
    {
      test1: "01",
      test2: "02",
      test3: "03",
      test4: "03",
      test5: "03"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    }
  ]
};

export const cardMockData = [
  {
    value: "520",
    label: "Transcation Count"
  },
  { value: "1,00,00", label: "Total Debit Amount" },
  {
    value: "2,00,00",
    label: "Total Credit Amount"
  },
  {
    value: "1,00,00",
    label: "Current Bank Balance"
  }
];
