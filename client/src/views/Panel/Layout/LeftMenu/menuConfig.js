export default [
  {
    url: "/transactions",
    label: "Transactions",
    role: [],
    permissions: ["NPS_TRANSACTIONS"],
    isExpanded: false,
    children: [
      {
        url: "/investment",
        label: "Investment",
        role: [],
        children: [
          {
            url: "/tier1",
            label: "Tier 1",
            role: []
          },
          {
            url: "/tier2",
            label: "Tier 2",
            role: []
          }
        ]
      },
      {
        url: "/redemption",
        label: "Redemption",
        role: [],
        children: [
          // {
          //   url: "/tier1",
          //   label: "Tier 1",
          //   role: []
          // },
          {
            url: "/tier2",
            label: "Tier 2",
            role: []
          }
        ]
      }
    ]
  },
  {
    url: "/revenue",
    label: "Revenue",
    role: [],
    permissions: ["REVENUE_GENERATED"],
    isExpanded: false,
    children: [
      {
        url: "/account-opening-fee",
        label: "Account Opening Fee",
        role: []
      },
      {
        url: "/payment-charges",
        label: "Payment Charges",
        role: []
      }
    ]
  },
  {
    url: "/bank-balance",
    label: "Bank balance",
    role: [],
    permissions: ["BANK_BALANCE"],
    isExpanded: false
  },
  {
    url: "/financial-recon",
    label: "Financial Recon",
    role: [],
    permissions: ["AUDITING_ENTRIES"],
    isExpanded: false
  },
  {
    url: "/reports",
    label: "Reports",
    role: [],
    permissions: ["REPORTS"],
    isExpanded: false
  }
];
