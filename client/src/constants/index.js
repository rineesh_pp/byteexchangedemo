export const BYTESCOPE_BASEURL = "/api";
export const AUTH_BASEURL = "/auth";
export const APP_TOKEN = "app_t";

export const apiURLConfig = {
  logoutUrl: `${AUTH_BASEURL}/v2/logout`,
  sessionReload: `${AUTH_BASEURL}/v2/reload-session`,
  gstReportUrl: `${BYTESCOPE_BASEURL}/reports/gst-payable`,
  trialBalanceReportUrl: `${BYTESCOPE_BASEURL}/reports/trial-balance`,
  transactionReportUrl: `${BYTESCOPE_BASEURL}/reports/transaction`,
  trialBalanceUrl: `${BYTESCOPE_BASEURL}/bank-balance`,
  finReconUrl: `${BYTESCOPE_BASEURL}/finance`,
  finHeadTypesUrl: `${BYTESCOPE_BASEURL}/finance/headtypes`,
  finEntryUrl: `${BYTESCOPE_BASEURL}/finance/entry`,
  transactionsUrl: `${BYTESCOPE_BASEURL}/transaction`
};

export const PAYMENT_INITIATED = "PAYMENTINITIATED";
export const PAYMENT_PENDING = "PAYMENTPENDING";
export const PAYMENT_FAILED = "PAYMENTFAILED";
export const PAYMENT_DONE = "PAYMENTDONE";
export const INITIATED = "INITIATED";
export const UPLOADED = "UPLOADED";
export const SUCCESS = "SUCCESS";
export const REVERSED = "REVERSED";
export const REJECTED = "REJECTED";
export const UPLOAD_REJECTED = "UPLOADREJECTED";
export const UPLOAD_FAILED = "UPLOADFAILED";
export const AMOUNT_CREDITED = "AMOUNTCREDITED";
export const PURCHASE = "PURCHASE";
export const REDEEM = "REDEEM";
export const SUBSEQUENT_PURCHASE = "SUBSEQUENT_PURCHASE";
export const TIER1 = "TIER1";
export const TIER2 = "TIER2";
export const REDEMPTION = "REDEMPTION";
export const REVENUE = "REVENUE";
export const PAYMENT_FEES = "PAYMENT_FEES";
export const REGISTRATION_FEES = "REGISTRATION_FEES";
// filter keys
export const USERID = "userId";
export const MASTER_TRANSACTION_ID = "masterTransactionId";
export const TAB_TYPE = "tabType";
export const PRAN = "pran";
export const ORDERID = "orderId";
export const ORDER_DISPLAY_ID = "orderDisplayId";
export const FROM_DATE = "fromDate";
export const TO_DATE = "toDate";
export const TRANSACTION_TYPES = "transactionTypes";
export const ACCOUNT_TYPES = "accountTypes";
export const TRANSACTION_STATUSES = "transactionStatuses";
export const PFM_CODES = "pfmCodes";
export const PAYMENT_STATUS_1 = "firstLegPaymentStatuses";
export const PAYMENT_STATUS_2 = "secondLegPaymentStatuses";
export const PAGE_NUMBER = "pageNumber";
export const PAGE_SIZE = "pageSize";
export const REVENUE_TYPE = "revenueType";

export const filterKeys = {
  TAB_TYPE,
  USERID,
  MASTER_TRANSACTION_ID,
  PRAN,
  ORDERID,
  ORDER_DISPLAY_ID,
  FROM_DATE,
  TO_DATE,
  TRANSACTION_TYPES,
  ACCOUNT_TYPES,
  TRANSACTION_STATUSES,
  PFM_CODES,
  PAYMENT_STATUS_1,
  PAYMENT_STATUS_2,
  PAGE_NUMBER,
  PAGE_SIZE,
  REVENUE_TYPE
};
export const initFilterParams = {
  [TAB_TYPE]: null,
  [USERID]: null,
  [MASTER_TRANSACTION_ID]: null,
  [PRAN]: null,
  [ORDERID]: null,
  [ORDER_DISPLAY_ID]: null,
  [FROM_DATE]: null,
  [TO_DATE]: null,
  [TRANSACTION_TYPES]: null,
  [ACCOUNT_TYPES]: null,
  [TRANSACTION_STATUSES]: null,
  [PFM_CODES]: null,
  [PAYMENT_STATUS_1]: null,
  [PAYMENT_STATUS_2]: null,
  [PAGE_NUMBER]: 0,
  [PAGE_SIZE]: 25,
  [REVENUE_TYPE]: null
};
export const transactionTypes = [
  {
    purchase: PURCHASE,
    redeem: REDEEM,
    subPurchase: SUBSEQUENT_PURCHASE
  }
];

export const accountTypes = {
  tier1: TIER1,
  tier2: TIER2
};
export const transactionTabs = {
  purchase: PURCHASE,
  redemption: REDEMPTION,
  revenue: REVENUE
};
export const revenueTypes = {
  paymentFees: PAYMENT_FEES,
  registrationFees: REGISTRATION_FEES
};

export const searchSelectOptions = [
  { name: "User Id", value: USERID },
  { name: "PRAN", value: PRAN },
  { name: "Transaction Id", value: MASTER_TRANSACTION_ID }
];
export const paymentStatusSelectOptions = [
  {
    name: "all",
    value: "all"
  },
  { name: "Payment initiated", value: PAYMENT_INITIATED },
  { name: "Payment pending", value: PAYMENT_PENDING },
  { name: "Payment failed", value: PAYMENT_FAILED },
  { name: "Payment done", value: PAYMENT_DONE }
];
export const transactionStatusSelectOptions = [
  {
    name: "all",
    value: "all"
  },
  { name: "Initiated", value: INITIATED },
  { name: "Uploaded", value: UPLOADED },
  { name: "Success", value: SUCCESS },
  { name: "Reversed", value: REVERSED },
  { name: "Rejected", value: REJECTED },
  { name: "Upload rejected", value: UPLOAD_REJECTED },
  { name: "Upload failed", value: UPLOAD_FAILED },
  { name: "Amount credited", value: AMOUNT_CREDITED }
];
export const pfmStatusSelectOptions = [
  {
    name: "all",
    value: "all"
  },
  { name: "PFM001", value: "PFM001" },
  { name: "PFM002", value: "PFM002" },
  { name: "PFM003", value: "PFM003" },
  { name: "PFM004", value: "PFM004" },
  { name: "PFM005", value: "PFM005" },
  { name: "PFM006", value: "PFM006" },
  { name: "PFM007", value: "PFM007" },
  { name: "PFM008", value: "PFM008" },
  { name: "PFM009", value: "PFM009" },
  { name: "PFM010", value: "PFM010" }
];
const ASC = "asc";
const DESC = "desc";
export const SORT_TYPES = {
  ASC,
  DESC
};
