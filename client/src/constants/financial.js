const INVESTMENT_AMOUNT_PAYABLE_TO_PFRDA_ACCURAL =
  "INVESTMENT_AMOUNT_PAYABLE_TO_PFRDA_ACCURAL";
const INVESTMENT_AMOUNT_PAYABLE_TO_PFRDA_PAYABLE =
  "INVESTMENT_AMOUNT_PAYABLE_TO_PFRDA_PAYABLE";
const PG_RECEIVABLE = "PG_RECEIVABLE";
const BANK_COLLECTION_ACCOUNT = "BANK_COLLECTION_ACCOUNT";
const BANK_REVENUE_ACCOUNT = "BANK_REVENUE_ACCOUNT";
const REVENUE_AC_OPENING_FEE = "REVENUE_AC_OPENING_FEE";
const REVENUE_TRANSACTION_CHARGES = "REVENUE_TRANSACTION_CHARGES";
const REVENUE_PERSISTANCE_FEE = "REVENUE_PERSISTANCE_FEE";
const GSTPAYABLE = "GSTPAYABLE";
const OTHER_ASSET = "OTHER_ASSET";
const OTHER_LIABILITY = "OTHER_LIABILITY";
const OTHER_INCOME = "OTHER_INCOME";
const OTHER_EXPENSES = "OTHER_EXPENSES";
const CHARGES = "CHARGES";
const CHARGES_PAYABLE = "CHARGES_PAYABLE";
// payloadKeys
const FINANCIALDATE = "financialDate";
const DEBITHEAD = "debitHead";
const DEBITAMOUNT = "debitAmount";
const CREDITHEAD = "creditHead";
const CREDITAMOUNT = "creditAmount";
const REMARKS = "remarks";

export const headTypeItemObj = {
  [INVESTMENT_AMOUNT_PAYABLE_TO_PFRDA_ACCURAL]: {
    name: "Investment amount payable to PFRDA accural",
    value: INVESTMENT_AMOUNT_PAYABLE_TO_PFRDA_ACCURAL
  },
  [INVESTMENT_AMOUNT_PAYABLE_TO_PFRDA_PAYABLE]: {
    name: "Investment amount payable to PFRDA payable",
    value: INVESTMENT_AMOUNT_PAYABLE_TO_PFRDA_PAYABLE
  },
  [PG_RECEIVABLE]: { name: "PG receivable", value: PG_RECEIVABLE },
  [BANK_COLLECTION_ACCOUNT]: {
    name: "Bank collection account",
    value: BANK_COLLECTION_ACCOUNT
  },
  [BANK_REVENUE_ACCOUNT]: {
    name: "Bank revenue account",
    value: BANK_REVENUE_ACCOUNT
  },
  [REVENUE_AC_OPENING_FEE]: {
    name: "Revenue Ac opening fee",
    value: REVENUE_AC_OPENING_FEE
  },
  [REVENUE_TRANSACTION_CHARGES]: {
    name: "Revenue transaction charges",
    value: REVENUE_TRANSACTION_CHARGES
  },
  [REVENUE_PERSISTANCE_FEE]: {
    name: "Revenue persistance fee",
    value: REVENUE_PERSISTANCE_FEE
  },
  [GSTPAYABLE]: { name: "GST payable", value: GSTPAYABLE },
  [OTHER_ASSET]: { name: "Other asset", value: OTHER_ASSET },
  [OTHER_LIABILITY]: { name: "Other liability", value: OTHER_LIABILITY },
  [OTHER_INCOME]: { name: "Other income", value: OTHER_INCOME },
  [OTHER_EXPENSES]: { name: "Other expenses", value: OTHER_EXPENSES },
  [CHARGES]: { name: "Charges", value: CHARGES },
  [CHARGES_PAYABLE]: { name: "Charges payable", value: CHARGES_PAYABLE }
};

export const entryPayloadKeys = {
  FINANCIALDATE,
  DEBITHEAD,
  DEBITAMOUNT,
  CREDITHEAD,
  CREDITAMOUNT,
  REMARKS
};

export default { entryPayloadKeys, headTypeItemObj };
