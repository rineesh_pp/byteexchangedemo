import { FORMAT_TYPES } from './'
export const financeRecordsMapper = {

    tableData: {
        rowHeader: [
            { id: 'financialDate', title: 'Date', isFormat: true, formatType: FORMAT_TYPES.DATE, sortable: true },
            { id: 'reconId', title: 'FInRecon ID', sortable: true },
            { id: 'debitHead', title: 'Debit Head', sortable: true },
            { id: 'debitAmount', title: 'Debit Amount', isFormat: true, formatType: FORMAT_TYPES.INR, sortable: true },
            { id: 'creditHead', title: 'Credit Head', sortable: true },
            { id: 'creditAmount', title: 'Credit Amount', isFormat: true, formatType: FORMAT_TYPES.INR, sortable: true },
            { id: 'remarks', title: 'Remarks', sortable: true },
            { id: 'reconUserName', title: 'log of who entered', sortable: true }
        ]
    }
}
