
import { FORMAT_TYPES } from './'
export const gstKeyMapper = [
    {
        id: 'invoiceNumber',
        title: 'Invoice No.'
    },
    {
        id: 'invoiceType',
        title: 'Invoice/ DN/ CN',
        default: 'Invoice',
        options: ['Invoice', 'DN', 'CN']
    },
    {
        id: 'invoiceDate',
        title: 'Invoice Date',
        type: FORMAT_TYPES.DATE
    },
    {
        id: 'transactionDate',
        title: 'Transaction Date',
        type: FORMAT_TYPES.DATE
    },
    {
        id: 'orderId',
        title: 'Transaction ID'
    },
    {
        id: 'customerName',
        title: 'Customer Name'
    },
    {
        id: 'userId',
        title: 'Customer Code'
    },
    {
        id: 'materialDesc',
        title: 'Material Description'
    },
    {
        id: 'netAmount',
        title: 'Net Amount',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'cgst',
        title: 'CGST',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'sgst',
        title: 'SGST',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'igst',
        title: 'IGST',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'totalGst',
        title: 'Total Amount',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'hsnCode',
        title: 'HSN Code'
    },
    {
        id: 'customerState',
        title: 'Bill to Party State of supply'
    },
    {
        id: 'partyGst',
        title: 'Bill to Party GST No.'
    }
]

export const transactionKeyMapper = [
    {
        id: 'transactionDate',
        title: 'Transaction created date',
        type: FORMAT_TYPES.DATE
    },
    {
        id: 'userId',
        title: 'User ID'
    },
    {
        id: 'orderId',
        title: 'Order ID'
    },
    {
        id: 'pran',
        title: 'PRAN Number'
    },
    {
        id: 'pan',
        title: 'PAN Numebr'
    },
    {
        id: 'schemePreference',
        title: 'Scheme Preference',
        isNestedKey: true,
        nestedKey: 'tier1SchemePreference.tier2SchemePreference'
    },
    {
        id: 'transactionStatus',
        title: 'Transaction Status',
        isNestedKey: true,
        nestedKey: 'tier1TransactionStatus.tier2TransactionStatus'
    },
    {
        id: 'trusteePaymentStatus',
        title: 'Payment Status'
    },
    {
        id: 'paymentMethod',
        title: 'Payment Method'
    },
    {
        id: 'tier1MasterTransactionId',
        title: 'Transaction ID 1'
    },
    {
        id: 'tier1SchemePreference',
        title: 'Txn 1 Tier'
    },
    {
        id: 'tier1Amount',
        title: 'Txn 1 Amount',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'tier1FundName',
        title: 'Txn 1 Fund Name'
    },
    {
        id: 'collectionPaymentStatus',
        title: 'Payment txn 1 status'
    },
    {
        id: 'tier2MasterTransactionId',
        title: 'Transaction ID 2'
    },
    {
        id: 'tier2SchemePreference',
        title: 'Txn 2 Tier'
    },
    {
        id: 'tier2Amount',
        title: 'Txn 2 Amount',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'tier2FundName',
        title: 'Txn 2 Fund Name'
    },
    {
        id: 'revenuePaymentStatus',
        title: 'Payment Txn 2 Status'
    },
    {
        id: 'invoiceNumber',
        title: 'Invoice number'
    },
    {
        id: 'revenueAmount',
        title: 'Revenue Total',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'registrationCharges',
        title: 'A/c Opening Fee',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'paymentFees',
        title: 'Payment Fee',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'contributionUtrDate',
        title: 'GST UTR Date for contribution',
        type: FORMAT_TYPES.DATE
    },
    {
        id: 'revenueUtrDate',
        title: 'UTR Date for revenue',
        type: FORMAT_TYPES.DATE
    },
    {
        id: 'contributionUtr',
        title: 'UTR for contribution',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'revenueUtr',
        title: 'UTR for revenue',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'trusteeUtr',
        title: 'UTR for transfer to TB',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'trusteeUtrDate',
        title: 'UTR date for transfer to TB',
        type: FORMAT_TYPES.DATE
    },
    {
        id: 'craRefId',
        title: 'CRA ID'
    },
    {
        id: 'aging',
        title: 'Aging (Current date - UTR 1 is received date)(only if > 0)'
    }
]
export const trialBalanceKeyMapper = [
    {
        id: 'date',
        title: 'Date',
        type: FORMAT_TYPES.DATE
    },
    {
        id: 'investmentAmountPayableToPFRDA',
        title: 'Investment Amount payable To PFRDA',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'accural',
        title: 'Accural',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'payable',
        title: 'Payable',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'pgreceivable',
        title: 'PG Receivable',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'bankBalance',
        title: 'Bank Balance',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'collection',
        title: 'Collection',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'revenue',
        title: 'Revenue',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'revenueBalance',
        title: 'Revenue Balance',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'acOpeningFee',
        title: 'A/c Opening Fee',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'paymentFee',
        title: 'Payment Fee',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'gstpayable',
        title: 'GST Payable',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'otherAsset',
        title: 'Other Asset',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'otherLiability',
        title: 'Other Liability',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'otherIncome',
        title: 'Other income',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'otherExpenses',
        title: 'Other expenses',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'charges',
        title: 'Charges',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'chargesPayable',
        title: 'Charges Payable',
        type: FORMAT_TYPES.INR
    },
    {
        id: 'revenuePersistenceFee',
        title: 'Revenue Persistence Fee',
        type: FORMAT_TYPES.INR
    }
]
