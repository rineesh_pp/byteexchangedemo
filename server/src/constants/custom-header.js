export const customHeader = {
    REQUEST_ID: 'x-request-id',
    USER_AGENT: 'x-user-agent',
    AUTHORIZATION: 'Authorization',
    SYS_TYPE: 'sys-type'
}
export const customHeaderKeys = Object.values(customHeader)
