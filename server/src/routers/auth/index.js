import express from 'express'
import otpBasedAuthController from '../../controllers/otpBasedAuthController'
import errorHandler from '../../services/errorHandler'
import { authMiddleWare } from '../../services/middlewareServ'
import { TOKEN_CONFIG } from '../../constants'

const router = express.Router()
router.use(authMiddleWare)
/* OTP BASED LOGIN ROUTES */
router.post('/v2/login', (req, res) => {
    const { userId, password } = req.body
    if (!userId || !password) {
        return res.status(400).send({ error: 'User Id or password missing' })
    }
    return otpBasedAuthController
        .loginAgent(res.locals.byteScope, userId, password)
        .then(response => {
            res.send(response)
        })
        .catch(error => res.status(error.status || 500).send(errorHandler(error)))
})

router.post('/v2/verify-otp', (req, res) => {
    const { otp, initialToken } = req.body
    return otpBasedAuthController
        .verifyOtp(res.locals.byteScope, initialToken, otp)
        .then(response => {
            res.send(response)
        })
        .catch(error => res.status(error.status || 500).send(errorHandler(error)))
})

router.post('/v2/reset-password', (req, res) => {
    const {
        credentials: { userId, password },
        firstTimerToken
    } = req.body
    return otpBasedAuthController
        .resetPassword(res.locals.byteScope, userId, password, firstTimerToken)
        .then(response => {
            res.send(response)
        })
        .catch(error => res.status(error.status || 500).send(errorHandler(error)))
})

router.get('/v2/reload-session', (req, res) => {
    const appToken = req.cookies[TOKEN_CONFIG.APP_TOKEN]
    if (!appToken) {
        return res.status(400).send({ error: 'Token is missing' })
    }

    return otpBasedAuthController
        .reloadSession(res.locals.byteScope, appToken)
        .then(response => res.send(response))
        .catch(error => res.status(error.status || 500).send(errorHandler(error)))
})

router.post('/v2/logout', (req, res, next) => {
    const { userId } = req.body
    if (!userId) {
        const err = new Error()
        err.status = 400
        err.displayMessage = 'User id is missing'
        return next(err)
    }

    return otpBasedAuthController
        .logoutAgent(res.locals.byteScope, userId)
        .then(() => {
            res.clearCookie(TOKEN_CONFIG.APP_TOKEN)
            res.clearCookie(TOKEN_CONFIG.INITIAL_TOKEN)
            res.status(200).send('logout is successful')
        })
        .catch(err => {
            next(err)
        })
})

module.exports = router
