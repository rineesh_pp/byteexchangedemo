import express from 'express'
import { panelMiddleware, tokenHandler } from '../../services/middlewareServ'
import reports from '../../controllers/reportsController'
import records from '../../controllers/recordsController'

const router = express.Router()
router.use(tokenHandler)
router.use(panelMiddleware)
router.use(reports)
router.use(records)

module.exports = router
