import { getBankBalanceData } from './api'
import errorHandler from '../../services/errorHandler'

const bankBalanceController = (req, res) => {
    return getBankBalanceData(req, res.locals)
        .then(response => res.send(response))
        .catch(error => res.status(error.status || 500).send(errorHandler(error)))
}
export default bankBalanceController
