import express from 'express'
import transactionController from './transactionController'
import balanceController from './balanceController'
import { financeRecordsController, addFinanceController, getFinanceHeadController } from './financeController'

const router = express.Router()
router.get('/transaction', transactionController)
router.get('/bank-balance', balanceController)
router.get('/finance', financeRecordsController)
router.get('/finance/headtypes', getFinanceHeadController)
router.post('/finance/entry', addFinanceController)

module.exports = router
