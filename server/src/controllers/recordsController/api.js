import apiService from '../../services/apiService'
import { CONFIG_ENV } from '../../config/environment'
import { commonTableMapper } from '../../constants/commonMapper'
import { financeRecordsMapper } from '../../constants/financeMapper'
import { formatTable, queryMap, sortResponse } from '../../utils'
import { filterConfig, initSort } from '../../constants'
import _ from 'lodash'

const {
    API: { byteExUrl }
} = CONFIG_ENV
const { tableData: { rowHeader: commonMapper } } = commonTableMapper
const { tableData: { rowHeader: financeMapper } } = financeRecordsMapper

const transactionUrl = `${byteExUrl}/transaction-tab`
const bankBalanceUrl = `${byteExUrl}/bank-balance-tab`
const financeRecordsUrl = `${byteExUrl}/financial/records`
const financeEntryUrl = `${byteExUrl}/financial/entry`
const financeHeadUrl = `${byteExUrl}/financial/headtypes`

const updateBalanceResponse = ({ data, meta }, sort = initSort.bankBalance) => {
    let updatedData = _.omit(data, ['transactions'])
    let sortedData = sortResponse({ sort, data: data.transactions })
    updatedData.tableData = formatTable({ data: sortedData, sort, mapper: commonMapper })
    return { data: updatedData, meta }
}
const updateTransactionResponse = ({ data, meta }, sort = initSort.transaction) => {
    let updatedData = _.omit(data, ['orders'])
    let sortedData = sortResponse({ sort, data: data.orders })
    updatedData.tableData = formatTable({ data: sortedData, sort, mapper: commonMapper })
    return { data: updatedData, meta }
}
const updateFinanceResponse = ({ data, meta }, sort = initSort.financial) => {
    let updatedData = _.omit(data, ['results'])
    let sortedData = sortResponse({ sort, data: data.results })
    updatedData.tableData = formatTable({ data: sortedData, sort, mapper: financeMapper })
    return { data: updatedData, meta }
}
const getTransactionData = (req, reqLocals) => {
    const { byteScope: headers } = reqLocals
    let { sort } = req.query
    const query = _.omit(req.query, ['sort'])
    let params = {
        ...query,
        ...queryMap(query, filterConfig)
    }
    return new Promise((resolve, reject) => {
        apiService
            .get({
                url: transactionUrl,
                headers,
                reqLocals,
                params
            })
            .then(res => {
                resolve(updateTransactionResponse(res, sort))
            }).catch(err => {
                reject(err)
            })
    })
}
const getBankBalanceData = (req, reqLocals) => {
    const { byteScope: headers } = reqLocals
    let { sort } = req.query
    const query = _.omit(req.query, ['sort'])
    let params = {
        ...query,
        ...queryMap(query, filterConfig)
    }
    return new Promise((resolve, reject) => {
        apiService
            .get({
                url: bankBalanceUrl,
                headers,
                reqLocals,
                params
            })
            .then(res => {
                resolve(updateBalanceResponse(res, sort))
            }).catch(err => {
                reject(err)
            })
    })
}

const getFinanceData = (req, reqLocals) => {
    const { byteScope: headers } = reqLocals
    let { sort } = req.query
    const query = _.omit(req.query, ['sort'])
    let params = {
        ...query,
        ...queryMap(query, filterConfig)
    }
    return new Promise((resolve, reject) => {
        apiService
            .get({
                url: financeRecordsUrl,
                headers,
                reqLocals,
                params
            })
            .then(res => {
                resolve(updateFinanceResponse(res, sort))
            }).catch(err => {
                reject(err)
            })
    })
}
const getFinanceHeadData = (req, reqLocals) => {
    const { byteScope: headers } = reqLocals
    return new Promise((resolve, reject) => {
        apiService
            .get({
                url: financeHeadUrl,
                headers,
                reqLocals
            })
            .then(res => {
                resolve(res)
            }).catch(err => {
                reject(err)
            })
    })
}
const postFinanceData = (req, reqLocals, reqHeaders) => {
    const { byteScope: headers } = reqLocals
    headers['Content-Type'] = 'application/json'
    headers['reconUserId'] = reqHeaders?.reconuserid
    headers['reconUserName'] = reqHeaders?.reconusername
    const data = JSON.stringify(req.body)
    return new Promise((resolve, reject) => {
        apiService
            .post({
                url: financeEntryUrl,
                headers,
                reqLocals,
                data
            })
            .then(res => {
                resolve(res)
            }).catch(err => {
                reject(err)
            })
    })
}
module.exports = {
    getTransactionData,
    getBankBalanceData,
    getFinanceData,
    postFinanceData,
    getFinanceHeadData
}
