import { getFinanceData, postFinanceData, getFinanceHeadData } from './api'
import errorHandler from '../../services/errorHandler'

const financeRecordsController = (req, res) => {
    return getFinanceData(req, res.locals)
        .then(response => res.send(response))
        .catch(error => res.status(error.status || 500).send(errorHandler(error)))
}
const addFinanceController = (req, res) => {
    return postFinanceData(req, res.locals, req.headers)
        .then(response => res.send(response))
        .catch(error => res.status(error.status || 500).send(errorHandler(error)))
}
const getFinanceHeadController = (req, res) => {
    return getFinanceHeadData(req, res.locals)
        .then(response => res.send(response))
        .catch(error => res.status(error.status || 500).send(errorHandler(error)))
}
export {
    financeRecordsController,
    addFinanceController,
    getFinanceHeadController
}
