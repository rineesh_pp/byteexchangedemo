import { getGstReports } from './api'
import errorHandler from '../../services/errorHandler'

const gstController = (req, res) => {
    return getGstReports(req, res.locals)
        .then(response => res.send(response))
        .catch(error => res.status(error.status || 500).send(errorHandler(error)))
}
export default gstController
