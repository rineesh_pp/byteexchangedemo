
import express from 'express'
import path from 'path'
import cors from 'cors'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import compression from 'compression'
import { ENV, CONFIG_ENV } from './config/environment'
import auth from './routers/auth'
import panel from './routers/panel'
import errorHandler from './services/errorHandler'
import morganBody from './utils/morganBody'
import {
    middlewareHeader,
    customForwardHeaders,
    loggerMiddleware,
    localsMiddleware
} from './services/middlewareServ'
import followRedirects from 'follow-redirects'
followRedirects.maxBodyLength = 1 * 1024 * 1024 * 1024// 1GB

const { PORT } = CONFIG_ENV
const rootPath = process.cwd().split('server')[0]
const app = express()
let staticPath = path.join(rootPath, 'client/dist')
app.use(express.static(staticPath))
app.use(compression())
app.use(bodyParser.json())
app.use(cookieParser())
app.use(cors())
app.use(localsMiddleware)
app.use(middlewareHeader)
app.use(customForwardHeaders)
app.use(loggerMiddleware)
morganBody(app)

app.listen(PORT, () => console.log(`The server is running at http://localhost:${PORT} ENV:${ENV}`))
console.info(
    `"/info" shows server health stats`,
    path.join(staticPath, 'index.html')
) // eslint-disable-line no-console
app.get('/info', require('express-healthcheck')()) // ping path
app.use('/auth', auth)
app.use('/api', panel)

app.use(errorHandler)
app.get('*', (req, res) => res.sendFile(path.join(staticPath, 'index.html')))

export default app
