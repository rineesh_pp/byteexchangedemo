// Flag to toggle preprod
const IS_PRE_PROD = false
const BASE_URL = {
    auth_v2: 'http://demo8698564.mockable.io',
    byteApi: 'http://demo8698564.mockable.io'
}

export default {
    PORT: '8080',
    HOST: 'http://localhost',
    API: {
        authUrl: `${BASE_URL.auth_v2}/pf/acl/v1`,
        byteExUrl: `${BASE_URL.byteApi}/byte-exchange`
    },
    IS_PRE_PROD
}
