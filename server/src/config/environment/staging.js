// Flag to toggle preprod
const IS_PRE_PROD = false

const BASE_URL = {
    auth_v2: '',
    byteApi: 'http://demo8698564.mockable.io'
}

export default {
    PORT: '8080',
    HOST: '',
    API: {
        authUrl: `${BASE_URL.auth_v2}/pf/acl/v1`,
        byteExUrl: `${BASE_URL.byteApi}/byte-exchange`
    },
    IS_PRE_PROD
}
