import moment from 'moment'
import omit from 'lodash/omit'
import sortBy from 'lodash/sortBy'
import jsonexport from 'jsonexport'
import { FORMAT_TYPES, SORT_TYPES } from '../constants'
const createTableData = (params) => {
    let { meta, data } = params
    let rowData = data.map((row) => {
        let dataArr = []
        dataArr = meta.rowVal.map((val) => {
            if (Array.isArray(val)) {
                let returnVal = val.map((valObj) => {
                    return !valObj.isFormat ? row[valObj.key] : formatValue({ value: row[valObj.key], isINR: valObj.isINR })
                })

                return returnVal.join('_')
            } else {
                return !val.isFormat ? row[val.key] : formatValue({ value: row[val.key], isINR: val.isINR })
            }
        })

        return dataArr
    })

    return { rowHeader: meta.rowHeader, rowData: rowData }
}
const timestampToDate = (timestamp) => {
    return timestamp ? moment.unix(timestamp / 1000).format('DD MMM YYYY') : 'NA'
}
const formatValue = ({ value, isINR = false }) => {
    let strVal = value ? value.toString() : '0'
    let newVal = strVal.split('.')[0]
    if (Number(newVal) === 0) return Number(newVal)
    if (newVal.length === 7) {
        newVal = newVal.substring(0, 4)
        newVal = (parseInt(newVal) / 100)
        newVal = newVal % 10 === 0 ? Math.round(newVal) : newVal
        newVal = `${newVal}L`
    } else if (newVal.length > 7) {
        newVal = newVal.substring(0, newVal.length - 5)
        newVal = (parseInt(newVal) / 100)
        newVal = newVal % 10 === 0 ? Math.round(newVal).toLocaleString('en-IN') : newVal.toLocaleString('en-IN')
        newVal = `${newVal}Cr`
    } else {
        newVal = parseInt(newVal).toLocaleString('en-IN').toString()
    }

    if (isINR) {
        newVal = `INR ${newVal}`
    }

    return newVal
}
const formatTable = ({ data = [], sort = null, mapper }) => {
    let header
    if (sort) {
        sort = JSON.parse(sort)
        header = mapper.map((obj) => {
            if (sort.sortKey === obj.id) {
                obj.sorted = true
                obj.sortOrder = sort.sortOrder
            } else {
                obj = omit(obj, ['sorted', 'sortOrder'])
            }
            return obj
        })
    } else {
        header = mapper
    }
    let rowData = data.map(row => {
        let mappedVal = {}
        header.forEach(obj => {
            const key = obj.id
            if (obj.isNestedKey) {
                const keys = obj.nestedKey.split('.')
                let value = []
                for (let i in keys) {
                    const k = keys[i]
                    if (row.hasOwnProperty(k) && row[k]) {
                        if (Number(i) === keys.length - 1) {
                            value.push(row[k])
                            break
                        }
                        value.push(row[k])
                    } else {
                        break
                    }
                }
                mappedVal[key] = value.join(',')
            } else {
                mappedVal[key] = row[key]
            }
            if (obj.isFormat) {
                if (obj.formatType === FORMAT_TYPES.INR) {
                    mappedVal[key] = row[key] ? formatValue({ value: row[key], isINR: true }) : 0
                }
                if (obj.formatType === FORMAT_TYPES.DATE) {
                    mappedVal[key] = row[key] ? timestampToDate(row[key]) : 'NA'
                }
                if (obj.formatType === FORMAT_TYPES.NUMBER) {
                    mappedVal[key] = row[key] ? Number(row[key]) : 'NA'
                }
            }
        })
        return mappedVal
    })

    const totalrows = rowData?.length
    return { rowHeader: header, rowData, totalRows: totalrows }
}
const getCurrentTime = () => {
    return moment().format('ddd HH:mm, Do MMM YYYY')
}
const apiStatus = (meta) => {
    let { code = '400000' } = meta
    if (code === 'PM_DF_SC_101') return 200

    const codeSplitRegex = /_|-/
    const codeArr = code.split(codeSplitRegex)
    if (codeArr.length > 1) {
        code = codeArr[codeArr.length - 1]
    } else {
        code = codeArr[0]
    }

    const status = (code.length >= 3 ? code.slice(0, 3) : 400)
    return Number(status)
}

const formatError = (meta) => {
    let { displayMessage, message } = meta
    const status = apiStatus(meta)
    return { status, displayMessage: displayMessage || message }
}
const queryMap = (data, queryMap, outputObject = {}) => {
    if (!data) return {}
    let mappedObject = { ...outputObject }
    Object.entries(queryMap).forEach(([outputKey, inputkey]) => {
        if (Object.prototype.hasOwnProperty.call(data, inputkey)) {
            mappedObject[outputKey] = data[inputkey]
        } else {
            const mappingFunction = queryMap[outputKey]
            if (typeof mappingFunction === 'function' && data[outputKey]) {
                mappedObject[outputKey] = mappingFunction(data)
            }
        }
    })
    return mappedObject
}
const convertToCsv = data => {
    return new Promise((resolve, reject) => {
        jsonexport(data, function (err, csv) {
            if (err) {
                console.error(err)
                reject(err)
            }
            resolve({ csv, count: data.length })
        })
    })
}
const sortResponse = ({ data = [], sort = '{}' }) => {
    const { sortKey, sortOrder } = JSON.parse(sort)
    let srtd = sortKey ? sortBy(data, (o) => o[sortKey]) : data
    return sortOrder === SORT_TYPES.DESC ? srtd.reverse() : srtd
}
export {
    createTableData,
    formatValue,
    formatTable,
    getCurrentTime,
    formatError,
    apiStatus,
    queryMap,
    convertToCsv,
    timestampToDate,
    sortResponse
}
