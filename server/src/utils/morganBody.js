const morganBody = (app) => {
    var originalSend = app.response.send
    app.response.send = function setBody (body) {
        originalSend.call(this, body)
        this.__morgan_body_response = body
    }
}

export default morganBody
